//
//  FirstViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/19/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit
import MediaPlayer

class FirstViewController: UIViewController {
    
    override func viewDidAppear(animated: Bool) {
        
        playVideo()
    }
    
    var moviePlayer : MPMoviePlayerController?
    
    func playVideo() {
        let path = NSBundle.mainBundle().pathForResource("sample_iPod", ofType:"m4v")
        let url = NSURL.fileURLWithPath(path!)
        moviePlayer = MPMoviePlayerController(contentURL: url)
        
        if let player = moviePlayer {
            
            player.view.frame = CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: 300)
            
            player.prepareToPlay()
            player.scalingMode = .AspectFill
            player.controlStyle = .None
            player.shouldAutoplay = true
            player.repeatMode = MPMovieRepeatMode.One
            self.view.addSubview(player.view)
            
        }    
    }
}
