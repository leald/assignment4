//
//  ThirdViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/19/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit
import MapKit

class ThirdViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                
        var location = CLLocationCoordinate2D(
            latitude: 44.46,
            longitude: -93.16
        )
        var span = MKCoordinateSpanMake(1, 1)
        var region = MKCoordinateRegion(center: location, span: span)
        
        map.setRegion(region, animated: true)
        
        
        
    }
    
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
}
