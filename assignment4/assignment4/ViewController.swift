//
//  ViewController.swift
//  assignment4
//
//  Created by mobiledev on 4/18/15.
//  Copyright (c) 2015 mobiledev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var animalsTableView: UITableView?
    
    
    let cellIdentifier = "AnimalsCell"
    let tableItems = ["Watch a video!", "Count to 100!", "Look at a map!"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animalsTableView?.delegate = self
        self.animalsTableView?.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell.textLabel?.text = self.tableItems[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator

        if(indexPath.row == 0) {
            navigateFirstCell()
        } else if (indexPath.row == 1) {
            navigateSecondCell()
        } else if (indexPath.row == 2) {
            navigateThirdCell()
        }
    }
    
    
    func navigateFirstCell() {
        //Handles navigation on a click of the first cell
        let storyboard = UIStoryboard(name: "First", bundle: nil)
        let firstVC = storyboard.instantiateViewControllerWithIdentifier("FirstViewController") as FirstViewController
        

        
        self.navigationController?.pushViewController(firstVC, animated: true)
    }
    
    func navigateSecondCell() {
        //Handles navigation on a click of the second cell
        let storyboard = UIStoryboard(name: "Second", bundle: nil)
        let secondVC = storyboard.instantiateViewControllerWithIdentifier("SecondViewController") as SecondViewController
        
        
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
    func navigateThirdCell() {
        //Handles navigation on a click of the third cell
        let storyboard = UIStoryboard(name: "Third", bundle: nil)
        let thirdVC = storyboard.instantiateViewControllerWithIdentifier("ThirdViewController") as ThirdViewController
        
        
        self.navigationController?.pushViewController(thirdVC, animated: true)
    }
}

